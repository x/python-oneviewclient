# -*- encoding: utf-8 -*-
#
# (c) Copyright 2015 Hewlett Packard Enterprise Development LP
# Copyright 2015 Universidade Federal de Campina Grande
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

# Power State

ONEVIEW_POWER_ON = 'On'

ONEVIEW_POWER_OFF = 'Off'

ONEVIEW_POWERING_ON = 'PoweringOn'

ONEVIEW_POWERING_OFF = 'PoweringOff'

ONEVIEW_RESETTING = 'Resetting'


# Server Hardware State

ONEVIEW_APPLYING_PROFILE = 'ApplyingProfile'

ONEVIEW_PROFILE_APPLIED = 'ProfileApplied'

ONEVIEW_REMOVING_PROFILE = 'RemovingProfile'

ONEVIEW_PROFILE_ERROR = 'ProfileError'

ONEVIEW_UNKNOWN = 'Unknown'


# Error State

ONEVIEW_ERROR = 'error'
