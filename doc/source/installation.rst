============
Installation
============

At the command line::

    $ pip install python-oneviewclient

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv python-oneviewclient
    $ pip install python-oneviewclient
